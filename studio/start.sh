#!/usr/bin/env ksh
# bash might work, but no guarentees

#	Mnemonic:	start
#	Abstract:	Start a studio container. Several direcories in the PWD are
#				mounted into the container:
#					./home	- mounted as home
#					./data  - mounted as /playpen/data
#					./projects - mounted as /projects
#
#				The container is started using the current user id/group and
#				setting the user name to that associated with $LOGNAME. This
#				should make all file ownerships right, though the name inside
#				of the container will not actually reflect as there is no
#				passwd entry change.
#
#				An attempt is made to allow the usb device(s) access from
#				the container and make debugging etc possible from studio,
#				but this is dodgy at best.
#
#				This script also cleans up exited containers as the default
#				is for docker to leave them lying round. (unless -k, keep)
#				is used on the command line.
#
#				NNOTE: 'xhost +' must be run to oepn the server up before
#				starting the container.
#
#	Author:		E. Scott Daniels
#	Date:		2019
# ----------------------------------------------------------------------

interactive=1
tag=studio:latest
user=${LOGNAME:-goober}
uid=$( id -u )
gid=$( id -g )
cleanup=1
cname=studio_$$

while [[ $1 == -* ]]
do
	case $1 in
		-d)	interactive=0;;
		-i)	interactive=1;;
		-k) cleanup=0;;
		-N)	cname=$2; shift;;
		-t)	tag=$2; shift;;
		-u)	user=$2; shift;;
		-U)	uid=${2%:*}; gid=${2:#*:}; shift;;

		*)	echo "unrecognised option: $1"
			echo "usage: $0 [{-d|i}] [-k] [-t image:tag] [-u usr-name] [-V studio-version]"
			echo "  -k (keep) do not cleanup exited containers, nor check for running container"
			echo "  -d (detach) run detached from the tty"
			echo "  -i (interactve) run attached to the tty"
			exit 1
			;;
	esac

	shift
done

if [[ ! -d home ]]
then
	echo "abort: no home directory found in $PWD"
	exit 1
fi

if [[ ! -d home/$user ]]
then
	if ! mkdir home/$user ]]
	then
		echo "unable to find or make $PWD/home/$user"
		exit 1
	fi
fi

if [[ ! -d data ]]
then
	mkdir data

	#mkdir -p data/profile/android
	#mkdir -p data/profile/gradle
	#mkdir -p data/profile/java
fi

if (( cleanup ))
then
	cname=studio
	docker ps -a|grep $cname | awk '		# find any copies of the container
		/Exited/  { print $1; next }
		{ print "running" }
	' |  while read id
	do
		if [[ $id == "running" ]]
		then
			echo "looks like container is running"
			docker ps -a|grep $cname
			exit
		fi
	
		docker rm $id	# clean up old exited one as docker is sloppy
	done
fi

if (( interactive ))
then
	detached="--rm -it"
	cmd=ksh
else
	detached="-d"
	cmd="/projects/bin/Studio"
fi

	#--device=/dev/usb/hiddev0 \
	#--user 1000:100 \
    #--net host \
set -x
docker run  $detached \
	--ipc=host \
    --cpuset-cpus 0 \
    --memory 2048mb \
	-v /dev/usb/hiddev0:/dev/usb/hidev0 \
	-v /dev/usb/hiddev1:/dev/usb/hidev1 \
	-v $PWD/projects:/projects \
	-v $PWD/home/$user:/home/$user \
	-v $PWD/data:/playpen/data \
    -v /tmp/.X11-unix:/tmp/.X11-unix\
    -e DISPLAY=unix$DISPLAY \
	-e HOME=/home/$user \
	-e LOGNAME=$user \
	-u ${uid}:${gid} \
    --name ${cname:-studio} \
	--privileged \
	--group-add plugdev \
	${tag:-studio:latest} ${1:-$cmd}

exit $?
