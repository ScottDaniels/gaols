#!/usr/bin/env ksh

#	Mnemonic:	start_studio.sh
#	Abstract:	Run inside of the container to start studio.
#				This should be copied into the image (/playpen/bin)
#				and can be run to start the GUI from inside of the
#				container.
#
#	Date:		24 May 2020
#	Author:		E. Scott Daniels
# -------------------------------------------------------------

if [[ $(id -u) == "0" ]]
then
    echo "switch to user (not root) before starting"
    exit 1
fi

if [[ -z $HOME ]]
then
	echo "env var HOME not defined"
	exit
fi

echo "Starting studio"
export GRADLE_USER_HOME=$HOME/.gradle

if [[ ! -d $HOME/? ]]
then
	mkdir $HOME/?
fi

cd $HOME/'?'
ksh /playpen/android-studio/bin/studio.sh >/dev/null 2>&1 &

